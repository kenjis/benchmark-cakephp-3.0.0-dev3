# CakePHP 3.0.0-dev3 for Benchmarking

## Installation

~~~
$ mkdir ~/tmp
$ cd ~/tmp
$ git clone git@bitbucket.org:kenjis/benchmark-cakephp-3.0.0-dev3.git
$ cd benchmark-cakephp-3.0.0-dev3
$ COMPOSER_PROCESS_TIMEOUT=3600 composer install
~~~

~~~
$ cd /path/to/htdocs
$ ln -s ~/tmp/benchmark-cakephp-3.0.0-dev3/webroot/ cake
~~~

## Benchmarking

~~~
$ siege -b -c 10 -t 3S http://localhost/cake/hello?name=BEAR
~~~
