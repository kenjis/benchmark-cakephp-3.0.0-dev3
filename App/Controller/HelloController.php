<?php

namespace App\Controller;

class HelloController extends AppController {
	public $uses = [];
	public $autoRender = false;

	public function index() {
		$name = $this->request->query('name') ?: 'World';
		echo 'Hello ' . $name . '!';
	}
}
